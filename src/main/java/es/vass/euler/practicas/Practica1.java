package es.vass.euler.practicas;

import java.util.stream.IntStream;

public class Practica1 {

	private static Integer TRES = 3;
	private static Integer CINCO = 5;
	private static Integer INICIO = 0;
	private static Integer FIN = 1000;
	
	public Integer multiplos() {
		return IntStream.range(INICIO,  FIN).boxed().filter(v -> (v%TRES== 0 || v%CINCO == 0)).reduce(0, Integer::sum);

	}
}
