package es.vass.euler.practicas;

import java.util.stream.Stream;

public class Practica2 {

	public Integer prueba() {

		return Stream.iterate(new Integer[] { 0, 1 }, t -> t[0] < 4000000, t -> new Integer[] { t[1], t[0] + t[1] })
				.filter(v -> (v[0] % 2 == 0)).mapToInt(v -> v[0]).sum();

	}
}
