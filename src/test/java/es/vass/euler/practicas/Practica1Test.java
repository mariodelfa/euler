package es.vass.euler.practicas;

import org.junit.jupiter.api.Test;

import junit.framework.Assert;
import junit.framework.TestCase;

public class Practica1Test extends TestCase {

	@Test
	public void multiploTest() {
		Practica1 p = new Practica1();

		Assert.assertEquals(233168, p.multiplos().intValue());
	}
}
